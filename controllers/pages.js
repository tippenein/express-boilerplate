
var pages = [];

function Page(uri, locals){
  this.uri    = uri;
  this.locals = locals;
  
  if (uri === '') {
    this.view = 'index.jade';
  } else {
    this.view = uri + '.jade';
  }
  Page.prototype.render = function(req) {
    // respond to the request with the rendered view
    req.res.render(this.view, this.locals)
  }
};
function page(uri, locals, type){
  // either passed a dict of locals or a string indicating the title
  if (typeof locals === 'string') {
    pages.push(new Page( uri, {title:locals, status:''})) 
  } else {
    pages.push(new Page( uri, locals));
  }
}

function route(app, page) {
  // give it a page instance and the app to route
  app.get('/' + page.uri, function(req) {
    page.render(req)
  });
};

/* Static pages (view, title)*/
page('', 'Title')

exports.setup = function(app) {
  /*setup static pages*/
  for (i in pages) {
    console.log(pages[i].locals)
    route(app, pages[i])
  };
};
