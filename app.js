/**
 * 
 */

var express = require('express')
  , app     = module.exports = express()

var config = require('./config.js')

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(config.staticDir));
  app.set('port', config.port);
  if (config.errorPages) {
    //Dev settings
    app.use(express.errorHandler({ dumpExceptions:true, showStack:true }));
    app.set('port', process.env.PORT || 8080);
    app.use(express.logger('dev'));
  } else {
    //Production settings
    app.use(express.errorHandler());
    app.use(function(req,res,next){
      res.status(404);
      res.render('404', {url: req.url, title: '404 - page cannot be found'});
    })
  }
});

// route controllers
controllers = ["pages", "admin"]
for (c in controllers) {
  require('./controllers/' + controllers[c]).setup(app);
}

app.listen(config.port);
